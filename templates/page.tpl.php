  <div id="page-wrapper"><div id="page">
    <div id="header-wrapper"><div id="header" class="page-wide-wrapper">
      <?php if ($logo): ?>
        <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
          <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
        </a>
      <?php endif; ?>
  
      <?php if ($site_name || $site_slogan): ?>
        <div id="name-and-slogan" class="clearfix <?php if ($hide_site_name && $hide_site_slogan) { print 'element-invisible'; } ?>">
  
          <?php if ($site_name): ?>
            <?php if ($title): ?>
              <div id="site-name"<?php if ($hide_site_name) { print ' class="element-invisible"'; } ?>>
                <strong>
                  <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><span><?php print $site_name; ?></span></a>
                </strong>
              </div>
            <?php else: /* Use h1 when the content title is empty */ ?>
              <h1 id="site-name"<?php if ($hide_site_name) { print ' class="element-invisible"'; } ?>>
                <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><span><?php print $site_name; ?></span></a>
              </h1>
            <?php endif; ?>
          <?php endif; ?>
  
          <?php if ($site_slogan): ?>
            <div id="site-slogan"<?php if ($hide_site_slogan) { print ' class="element-invisible"'; } ?>><div id="site-slogan-inside">
              <?php print $site_slogan; ?>
            </div></div>
          <?php endif; ?>
  
        </div> <!-- /#name-and-slogan -->
      <?php endif; ?>
      
      <?php if ($main_menu): ?>
        <div id="main-menu" class="navigation">
          <?php print theme('links__system_main_menu', array(
            'links' => $main_menu,
            'attributes' => array(
              'id' => 'main-menu-links',
              'class' => array('links','inline', 'clearfix'),
            ),
            'heading' => array(
              'text' => t('Main menu'),
              'level' => 'h2',
              'class' => array('element-invisible'),
            ),
          )); ?>
        </div> <!-- /#main-menu -->
      <?php endif; ?>
  
      <?php if ($secondary_menu): ?>
        <div id="secondary-menu" class="navigation">
          <?php print theme('links__system_secondary_menu', array(
            'links' => $secondary_menu,
            'attributes' => array(
              'id' => 'secondary-menu-links',
              'class' => array('links', 'inline', 'clearfix'),
            ),
            'heading' => array(
              'text' => t('Secondary menu'),
              'level' => 'h2',
              'class' => array('element-invisible'),
            ),
          )); ?>
        </div> <!-- /#secondary-menu -->
      <?php endif; ?>      
    </div></div> <!-- /#header, /#header-wrapper -->
    
    <div id="featured-wrapper"><div id="featured" class="page-wide-wrapper">

    </div></div> <!-- /#featured, /#featured-wrapper -->
    
    <div id="main-wrapper"><div id="main" class="<?php print $page['sidebar'] ? 'with-sidebar': 'without-sidebar'; ?> page-wide-wrapper clearfix">

      <div id="content-wrapper"><div id="content">
        <?php print $breadcrumb; ?>
        <?php if ($page['highlighted']): ?><div id="highlighted"><?php print render($page['highlighted']); ?></div><?php endif; ?>
        <a id="main-content"></a>
        <?php if ($tabs): ?><div id="tabs-wrapper" class="clearfix"><?php endif; ?>
        <?php print render($title_prefix); ?>
        <?php if ($title): ?>
          <h1<?php print $tabs ? ' class="with-tabs"' : '' ?>><?php print $title ?></h1>
        <?php endif; ?>
        <?php print render($title_suffix); ?>
        <?php if ($tabs): ?><?php print render($tabs); ?></div><?php endif; ?>
        <?php print render($tabs2); ?>
        <?php print $messages; ?>
        <?php print render($page['help']); ?>
        <?php if ($action_links): ?><ul class="action-links"><?php print render($action_links); ?></ul><?php endif; ?>
        <div class="clearfix">
          <?php print render($page['content']); ?>
        </div>  
      </div></div> <!-- /#content, /#content-wrapper -->

      <?php if ($page['sidebar']): ?>
        <div id="sidebar-wrapper"><div id="sidebar">
          <?php print render($page['sidebar']); ?>
        </div></div> <!-- /#sidebar, /#sidebar-wrapper -->
      <?php endif; ?>

    </div></div> <!-- /#main, /#main-wrapper -->
    
    <div id="footer-wrapper"><div id="footer" class="page-wide-wrapper">

    </div></div> <!-- /#footer, /#footer-wrapper -->    
  </div></div> <!-- /#page, /#page-wrapper -->